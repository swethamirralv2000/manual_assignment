package day_3_String;

import java.util.Scanner;

public class Assignment_Palindrome {

	public static void main(String[] args) {
		System.out.println("Enter a string ");
		String a,b="";
		Scanner sc=new Scanner(System.in);
		a=sc.next();
        int l=a.length();
        for(int k=l-1;k>=0;k--) {
        	b=b+a.charAt(k);
        }
         if(a.equalsIgnoreCase(b)) {
        	 System.out.println("The string is palindrome");
         }
         else {
        	 System.out.println("The string is not a palindrome");
         }
         sc.close();
	}

}
